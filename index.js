const puppeteer = require('puppeteer');
// const devices = require('puppeteer/DeviceDescriptors');
// const iPhone = devices[ 'iPhone 8' ];
const args = process.argv.slice(2);
const url = args[0];

const takeScreenshot = async() => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const options = {
        path: 'images/mobile.png',
        fullPage: true,
        omitBackground: true
    };
    // await page.emulate(iPhone);

    await page.goto(url);
    
    await page.setViewport({width: 375, height: 667, deviceScaleFactor: 1});
    await page.screenshot(options);

    await page.setViewport({width: 768, height: 1024, deviceScaleFactor: 1});
    await page.screenshot({...options, path:'images/ipad.png'});

    await page.setViewport({width: 1024, height: 1366, deviceScaleFactor: 1});
    await page.screenshot({...options, path:'images/ipadPro.png'});

    await page.setViewport({width: 1200, height: 1366, deviceScaleFactor: 1});
    await page.screenshot({...options, path:'images/desktop.png'});
    
    await page.close();
    await browser.close();
}


takeScreenshot();